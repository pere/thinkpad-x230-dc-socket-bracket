Holding bracket for Thinkpad X230 DC power socket
=================================================

A FreeCAD model of a bracket to hold the DC power socket in place incide the
machine. This try to compensate for when the small plastic tabs normally
holding the DC socket in place have broken off.

Designed using FreeCAD, currently version 0.19 and 0.20.

Latest version can be found on
[Codeberg](https://codeberg.org/pere/thinkpad-x230-dc-socket-bracket).
